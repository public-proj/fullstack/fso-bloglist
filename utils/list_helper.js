const _ = require('lodash');

const dummy = (blogs) => {
  return 1;
};

const totalLikes = (blogs) => {
  return blogs.reduce((sum, blog) => {
    return sum + blog.likes;
  }, 0);
};

const favouriteBlog = (blogs) => {
  const maxLike = Math.max(...blogs.map(({ likes }) => likes));
  // find the entry with highest like
  const found = blogs.filter((blog) => blog.likes === maxLike);
  const favBlog = {
    title: found[0].title,
    author: found[0].author,
    likes: found[0].likes,
  };
  return favBlog;
};

// TODO: No test yet
const mostBlogs = (blogs) => {
  const groupedByAuthor = _.groupBy(blogs, 'author');
  const authorPostCount = _.mapValues(groupedByAuthor, (o) => o.length);

  // author name
  const topAuthorName = _.maxBy(
    Object.keys(authorPostCount),
    (o) => authorPostCount[o]
  );

  // post count
  const topAuthorPostCount = authorPostCount[topAuthorName];

  const topAuthor = {
    author: topAuthorName,
    blogs: topAuthorPostCount,
  };

  return topAuthor;
};

const mostLikes = (blogs) => {
  const groupedByAuthor = _.groupBy(blogs, 'author');
  const likeReducer = (sum, blog) => sum + blog.likes;

  const authorByLikes = _.mapValues(groupedByAuthor, (o) =>
    o.reduce(likeReducer, 0)
  );

  // something wrong here
  const topAuthorNameByLike = _.maxBy(
    Object.keys(authorByLikes),
    (o) => authorByLikes[o]
  );

  const topAuthorLikeCount = authorByLikes[topAuthorNameByLike];

  const topAuthorByLikes = {
    author: topAuthorNameByLike,
    likes: topAuthorLikeCount,
  };

  return topAuthorByLikes;
};

module.exports = {
  dummy,
  totalLikes,
  favouriteBlog,
  mostBlogs,
  mostLikes,
};
