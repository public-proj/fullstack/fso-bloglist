require('dotenv').config();

const dbUser = process.env.DBUSER;
const dbPass = process.env.DBPASS;
const dbName = process.env.DBNAME;

const PORT = process.env.PORT;
let MONGODB_URI = `mongodb+srv://${dbUser}:${dbPass}@fullstackopen.koxae.mongodb.net/${dbName}>?retryWrites=true&w=majority`;
const TEST_MONGODB_URI = `mongodb+srv://${dbUser}:${dbPass}@fullstackopen.koxae.mongodb.net/${dbName}-test>?retryWrites=true&w=majority`;

if (process.env.NODE_ENV === 'test') {
  MONGODB_URI = TEST_MONGODB_URI;
}

console.log(`URI-${MONGODB_URI}`)

module.exports = {
  MONGODB_URI,
  PORT,
};
